### 数据分析

#### 2021.7.30
 * 背景：日常工作中常常会统计多种主体间的关联并画图展示。本题一共25分。
 * 题目说明：统计指定细胞类型间（Epithelium与Mac_C1-C5间）含有显著互作的样本数>1的基因对数目形成统计文件“circos_input.txt”，并在自己windows下安装R包circlize进行结果的展示，生成“circos.png”。  注意：A与B的互作和B与A的互作是不能合并的，要分开统计
   * significant_result.xls为互作的信息  表头含义：celltype为含有互作的细胞类型对，以|分隔；interacting_pair为互作的基因对；receptor为基因是否为受体（此处分析可忽略这一列）；sample1-6的数值代表样本中的互作强度，没有数值代表互作不显著。

 * 要求：
   * 在自己的电脑安装R，然后安装circlize包进行分析，相关代码和图片再上传到集群。
   * 生成ReadMe.txt, 里面描述对这个图的简单说明。(5分)
   * 对图片进行颜色调整或者其他美观的调整，视情况进行额外加分5~10分。


 * circos_input.txt示例  
   celltype1	celltype2	num  
   A	B	10  
   B	A	5  

 * circos.png图例子
   ![Image text](https://gitlab.com/111kakaluote/singleron/-/raw/main/20210730/circos_demo.png)
